# CHANGELOG

## 0.2.0
### Added
* `subHSetHReaderT` run a local reader with a subset of HSet elements
### Changed
* minimum required `hset` version is `1.1.0`

## 0.1.0
### Changed
* Use `hset-1.0.0`

## 0.0.2
Make it compilable on base-4.7

## 0.0.1
First working version
